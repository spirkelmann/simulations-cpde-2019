import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import configparser as cp
import sys

from pathlib import Path

linewidth_global = 2.0
label_fontsize = 30
legend_fontsize = 20

class SimulationResult:
    def __init__(self, results_folder):

        self.result_folder = results_folder

        p = Path(results_folder)

        q = p / 'sampling_rate.txt'
        if q.exists():
            self.sampling_rate = np.loadtxt(str(q), ndmin=1)[0]

        q = p / 'closedloop_t.txt'
        if q.exists():
            self.t_cl = np.fliplr(np.loadtxt(str(q), ndmin=2))

        q = p / 'closedloop_y.txt'
        if q.exists():
            self.y_cl = np.fliplr(np.loadtxt(str(q), ndmin=2))

        q = p / 'closedloop_u.txt'
        if q.exists():
            self.u_cl = np.loadtxt(str(q), ndmin=1)

        q = p / 'closedloop_w.txt'
        if q.exists():
            self.w_cl = np.loadtxt(str(q), ndmin=1)

        q = p / 'closedloop_cost.txt'
        if q.exists():
            self.closed_loop_cost = np.loadtxt(str(q), ndmin=1)
            self.final_closed_loop_cost = self.closed_loop_cost[-1]

        self.L = self.y_cl.shape[0]-1

        # load open loops
        open_loops_y = list(p.glob('openloop_y_k=*'))
        if len(open_loops_y) > 0:
            self.y_ol = []
        for k in range(0, len(open_loops_y)):
            file_str = str(open_loops_y[k])
            index_k_before = file_str.find('y_k=')
            index_k_after = file_str.find('.txt')
            time_index = int(file_str[index_k_before+4:index_k_after])
            q = p / ('openloop_y_k='+str(time_index)+'.txt')
            if q.exists():
                self.y_ol.append(np.fliplr(np.loadtxt(str(q), ndmin=1)))

        open_loops_u = list(p.glob('openloop_u_k=*'))
        if len(open_loops_u) > 0:
            self.u_ol = []
        for k in range(0, len(open_loops_u)):
            file_str = str(open_loops_u[k])
            index_k_before = file_str.find('u_k=')
            index_k_after = file_str.find('.txt')
            time_index = int(file_str[index_k_before + 4:index_k_after])
            q = p / ('openloop_u_k='+str(time_index)+'.txt')
            if q.exists():
                self.u_ol.append(np.loadtxt(str(q), ndmin=1))

        open_loops_w = list(p.glob('openloop_w_k=*'))
        if len(open_loops_w) > 0:
            self.w_ol = []
        for k in range(0, len(open_loops_w)):
            file_str = str(open_loops_w[k])
            index_k_before = file_str.find('w_k=')
            index_k_after = file_str.find('.txt')
            time_index = int(file_str[index_k_before + 4:index_k_after])
            q = p / ('openloop_w_k='+str(time_index)+'.txt')
            if q.exists():
                self.w_ol.append(np.loadtxt(str(q), ndmin=1))

        q = p / 'openloop_cost.txt'
        if q.exists():
            self.open_loop_cost = np.loadtxt(str(q), ndmin=1)

        # find parameters
        config = cp.ConfigParser()
        q = p / 'parameters.txt'
        if q.exists():
            config.read(str(q))

            self.n_disc = config.getint('param', 'discretization_parameter')
            self.N = config.getint('param', 'MPC_horizon')
            self.alpha = config.getfloat('param', 'alpha')
            self.beta = config.getfloat('param', 'beta')
            self.y_ref = config.getfloat('param', 'y_ref')
            self.gamma = config.getfloat('param', 'gamma')
            self.u_ref = config.getfloat('param', 'u_ref')
            self.u_upper = config.getfloat('param', 'u_upper')
            self.u_lower = config.getfloat('param', 'u_lower')
            self.y_upper = config.getfloat('param', 'y_upper')
            self.y_lower = config.getfloat('param', 'y_lower')
            self.w_upper = config.getfloat('param', 'w_upper')
            self.w_lower = config.getfloat('param', 'w_lower')
            self.convection = config.getboolean('param', 'convection')
            self.function = config.get('param', 'function')
        else:
            print("can't find parameters.txt")
            sys.exit()


    def plot_closed_loop(self, output_file, reference=None, L=None):
        if L is None:
            L = self.L
        else:
            L = min(L, self.L)
        FFMpegWriter = manimation.writers['ffmpeg']
        metadata = dict(title='closed_loop_N='+str(self.N), artist='Matplotlib',
                    comment='Movie support!')
        writer = FFMpegWriter(fps=15, metadata=metadata)


        h = 1.0/(self.n_disc)
        domain = np.arange(0.0, 1.0+0.5*h, h)
        subdomain = np.arange(0.25, 0.75+0.5*h, h)
        fig, ax = plt.subplots()
        #ax.hold(False)

        with writer.saving(fig, output_file, 100):
            for i in range(0, L):
                ax.plot(domain, self.y_cl[i], color='k', linewidth=linewidth_global, label='$y\;(N=' + str(self.N) + ')$')

                subdivisions = 20
                for j in range(0, subdivisions):
                    k = (self.n_disc-1)/subdivisions * j
                    k1 = (self.n_disc-1)/subdivisions * (j+1)
                    lx = (self.n_disc-1)/subdivisions * h
                    ly = self.y_cl[i][k1] - self.y_cl[i][k]
                    if self.w_cl[i] <= 0.0:
                        px1 = h * k
                        py1 = self.y_cl[i][k]
                        px2 = lx
                        py2 = ly
                    else:
                        px1 = h * k1
                        py1 = self.y_cl[i][k1]
                        px2 = -lx
                        py2 = -ly
                    head_width = ly * min(self.w_cl[i], 0.5) * 5
                    head_length = lx/2 * min(self.w_cl[i], 0.5) * 5
                    overhang = 0.9
                    head_lr = False

                    ax.arrow(px1, py1, px2, py2, head_width=head_width, head_length=head_length, fc='k',
                             ec='b', overhang=overhang, length_includes_head=True, head_starts_at_zero=head_lr)

                ax.hold(True)
                # plot constraints
                ax.plot(subdomain, self.y_lower * np.ones(subdomain.shape), color='r', linewidth=linewidth_global)
                ax.plot(subdomain, self.y_upper * np.ones(subdomain.shape), color='r', linewidth=linewidth_global)

                #ax.plot([0.95, 1.0], [self.u_upper, self.u_upper], color='b', linewidth=linewidth_global)
                #ax.plot([0.95, 1.0], [self.u_lower, self.u_lower], color='b', linewidth=linewidth_global)

                # plot text with time
                ax.text(0.8, 0.4, 't = {:5.3f}'.format((i)*self.sampling_rate), bbox={'facecolor':'white', 'pad':10})
                ax.hold(False)

                if reference:
                    ax.hold(True)
                    ax.plot(domain, reference.y_ol[0][i], color='g', linewidth=linewidth_global, label='$y^*$')
                    ax.hold(False)

                ax.set_xlim([0.0, 1.0])
                ax.set_ylim([-0.5, 0.5])

                ax.set(xlabel='$\Omega$', ylabel='$y$')
                ax.xaxis.label.set_size(20)
                ax.yaxis.label.set_size(20)

                ax.legend(loc='upper left')
                #plt.show()
                writer.grab_frame()

    def plot_open_loop(self,output_file,reference=None,k=0):
        FFMpegWriter = manimation.writers['ffmpeg']
        metadata = dict(title='open_loop_N='+str(self.N)+'_k='+str(k), artist='Matplotlib',
                    comment='Movie support!')
        writer = FFMpegWriter(fps=15, metadata=metadata)


        h = 1.0/(self.n_disc)
        domain = np.arange(0.0, 1.0+0.5*h, h)
        subdomain = np.arange(0.25, 0.75+0.5*h, h)
        fig, ax = plt.subplots()

        with writer.saving(fig, output_file, 100):
            for i in range(0, self.N):
                ax.plot(domain, self.y_ol[k][i], color='k', linewidth=linewidth_global, label='$y_{ol} \; (N=' + str(self.N) +')$')

                # subdivisions = 20
                # for j in range(0, subdivisions):
                #     k = (self.n_disc-1)/subdivisions * j
                #     k1 = (self.n_disc-1)/subdivisions * (j+1)
                #     lx = (self.n_disc-1)/subdivisions * h
                #     ly = self.y_ol[i][k1] - self.y_ol[i][k]
                #     if self.w_ol[i] <= 0.0:
                #         px1 = h * k
                #         py1 = self.y_ol[i][k]
                #         px2 = lx
                #         py2 = ly
                #     else:
                #         px1 = h * k1
                #         py1 = self.y_ol[i][k1]
                #         px2 = -lx
                #         py2 = -ly
                #     head_width = ly * min(self.w_ol[i], 0.5) * 5
                #     head_length = lx/2 * min(self.w_ol[i], 0.5) * 5
                #     overhang = 0.9
                #     head_lr = False
                #
                #     ax.arrow(px1, py1, px2, py2, head_width=head_width, head_length=head_length, fc='k',
                #              ec='b', overhang=overhang, length_includes_head=True, head_starts_at_zero=head_lr)

                ax.hold(True)
                # plot constraints
                ax.plot(subdomain, self.y_lower * np.ones(subdomain.shape), color='r', linewidth=linewidth_global)
                ax.plot(subdomain, self.y_upper * np.ones(subdomain.shape), color='r', linewidth=linewidth_global)

                #ax.plot([0.95, 1.0], [ub_u, ub_u], color='b', linewidth=linewidth_global)
                #ax.plot([0.95, 1.0], [lb_u, lb_u], color='b', linewidth=linewidth_global)

                # plot text with time
                ax.text(0.8, 0.4, 't = {:5.3f}'.format(i*self.sampling_rate), bbox={'facecolor':'white', 'pad':10})
                ax.hold(False)

                if reference:
                    ax.hold(True)
                    ax.plot(domain, reference.y_ol[k][i], color='g', linewidth=linewidth_global, label='$y^*$')
                    ax.hold(False)

                ax.set_xlim([0.0, 1.0])
                ax.set_ylim([-0.5, 0.5])

                ax.set(xlabel='$\Omega$', ylabel='$y$')
                ax.xaxis.label.set_size(20)
                ax.yaxis.label.set_size(20)

                ax.legend(loc='upper left')
                plt.show()
                writer.grab_frame()

    def plot_closed_loop_cost(self):
        domain = np.arange(0.0, self.L)
        fig, ax = plt.subplots()
        ax.hold(False)

        ax.plot(domain, self.closed_loop_cost)
        plt.show()

    # def plot_open_loop(self):
    #     domain = np.arange(0.0, 1.0, 1.0/self.n_disc)
    #     fig, ax = plt.subplots()
    #     ax.hold(False)
    #
    #     for i in range(0, self.N):
    #         ax.plot(domain, self.y_ol[i])
    #         ax.set_xlim([0.0, 1.0])
    #         ax.set_ylim([-0.5, 0.5])
    #         plt.show()
    #     pass

def plot_closed_loop_convergence(results, reference, output_file='test.pdf'):
    fig, ax = plt.subplots()
    ax.hold(True)

    from fenics import UnitIntervalMesh, FunctionSpace, Function, norm, plot
    mesh = UnitIntervalMesh(100)
    U = FunctionSpace(mesh, "Lagrange", 1)
    y1 = Function(U)
    y2 = Function(U)
    ydiff = Function(U)

    for r in results:
        y = r.y_cl
        y_ref = reference.y_ol[0]

        norm_diffs1 = []
        norm_diffs2 = []
        for j in range(0,r.L):
            y1.vector().set_local(y[j])
            y2.vector().set_local(y_ref[j])
            ydiff.assign(y1-y2)

            norm1 = norm(ydiff)
            norm2 = np.linalg.norm(y[j] - y_ref[j])

            print("norms: {} {}".format(norm1, norm2))

            norm_diffs1.append(norm1)
            norm_diffs2.append(norm2)

        domain = np.arange(0, 0+r.L)
        ax.plot(domain, norm_diffs1, label='N='+str(r.N), linewidth=linewidth_global)
        ax.set_xlim([0.0, r.L-1])
        #ax.plot(domain, norm_diffs2, label='N=' + str(r.N), linewidth=linewidth_global)

    plt.legend(loc='upper right')
    plt.xlabel('$k$', fontsize=20)
    plt.ylabel('$\|y_{\mu_{N}}(k,x) - y^*(k)\|_{L^2(\Omega)}$', fontsize=20)
    #plt.title('Convergence of MPC trajectories')
    plt.savefig(output_file)
    plt.show()

def plot_cumulative_closed_loop_cost(results, output_file='test.pdf'):
    fig, ax = plt.subplots()
    ax.hold(True)

    for r in results:
        domain = np.arange(0.0, r.L)
        ax.plot(domain, r.closed_loop_cost, label='N='+str(r.N), linewidth=linewidth_global)

    plt.legend(loc='upper left')
    plt.xlabel('$L$', fontsize=20)
    plt.ylabel('$J^{cl}_{L}(y,\mu_N)$', fontsize=20)
    #plt.title('Cumulative closed-loop cost')
    plt.savefig(output_file)
    plt.show()

def plot_cost_convergence(results, output_file='test.pdf'):
    L = 0
    Ns = []
    Js = []
    for r in results:
        Ns.append(r.N)
        Js.append(r.final_closed_loop_cost)

        L = max(L, r.L)
    Ns, Js = zip(*sorted(zip(Ns, Js)))

    fig, ax = plt.subplots()
    ax.hold(False)
    ax.plot(Ns, Js, linewidth=linewidth_global)
    ax.set_xlim([np.min(Ns), np.max(Ns)])

    #plt.legend(loc='best')
    plt.xlabel('$N$', fontsize=20)
    plt.ylabel('$J^{cl}_{' + str(L) + '}(y,\mu_N)$', fontsize=20)
    #plt.title('Convergence of closed-loop cost')
    plt.savefig(output_file)
    plt.show()

def plot_turnpike_ys(results, reference, k=0, output_file='test.pdf'):
    # turnpike plots for solutions with different initial values
    fig, ax = plt.subplots(figsize=(13,8))
    ax.hold(True)

    N = results[0].N
    y_ref = reference.y_ol[0][k:k+N+1]
    for r in results:
        y = r.y_ol[0]

        norm_diffs = []
        for j in range(0,r.N+1):
            norm_diffs.append(np.linalg.norm(y[j] - y_ref[0+j]))

        h_ = r.sampling_rate
        ts = np.arange(k * h_, (k + r.N + 1) * h_, h_)

        ax.plot(ts, norm_diffs, label='$y_0={:5.2f}$'.format(r.y_cl[0][0]), linewidth=linewidth_global)
        ax.set_xlim(k * r.sampling_rate, (k + r.N) * r.sampling_rate)

    plt.grid('on')
    plt.legend(loc='best', fontsize=legend_fontsize)
    plt.xlabel('Time $t$', fontsize=label_fontsize)
    plt.ylabel('$\|y_{u^*_{N}}(t) - \\tilde{{y}}^*_L(t)\|_{L^2(\Omega)}$', fontsize=label_fontsize)
    plt.tight_layout()
    plt.savefig(output_file)
    plt.show()

def plot_turnpike_cl(r, reference, output_file='test.pdf', max_k=None):
    # turnpike plots for solution with one initial value over multiple time steps + closed loop
    fig, ax = plt.subplots()
    ax.hold(True)

    if max_k is None:
        L = r.L
        max_k = r.L + r.N
    else:
        L = min(max_k - r.N, r.L)

    y_ref = reference.y_ol[0]

    norm_diffs_cl = []
    for k in range(0, L):
        y_ol = r.y_ol[k]
        y_cl = r.y_cl[k]

        norm_diffs_ol = []
        for j in range(0, r.N + 1):
            norm_diffs_ol.append(np.linalg.norm(y_ol[j] - y_ref[k + j]))

        norm_diffs_cl.append(np.linalg.norm(y_cl - y_ref[k]))

        domain = np.arange(k, k + r.N + 1)
        ax.plot(domain, norm_diffs_ol, linewidth=linewidth_global, color='black', linestyle='--')
    domain = np.arange(0, L)
    ax.plot(domain, norm_diffs_cl, linewidth=linewidth_global, color='red', linestyle='-')

    ax.set_xlim(0, max_k)
    plt.grid()
    plt.xlabel('$k$', fontsize=20)
    plt.ylabel('$\|y_{u^*_{'+ str(r.N) + '}}(k,x) - y^*(k)\|_{L^2(\Omega)}$', fontsize=20)
    plt.savefig(output_file)
    plt.show()

def plot_turnpikes_Ns(rs, reference, k=0, output_file='test.pdf'):
    # turnpike plot for solution with single initial value and different horizon lengths
    fig, ax = plt.subplots(figsize=(13,8))
    ax.hold(True)

    max_N = max([res.N for res in rs])

    y_ref = reference.y_ol[0][k:k+max_N+1]
    for res in rs:
        y_ol = res.y_ol[0]
        norm_diffs_ol = []
        for j in range(0, res.N + 1):
            norm_diffs_ol.append(np.linalg.norm(y_ol[j] - y_ref[j]))
        h_ = res.sampling_rate
        ts = np.arange(k * h_, (k + res.N + 1) * h_, h_)

        ax.plot(ts, norm_diffs_ol, label='$N={}$'.format(res.N), linewidth=linewidth_global)
        ax.set_xlim(k * res.sampling_rate, (k+res.N) * res.sampling_rate)

    plt.legend(loc='upper right', fontsize=legend_fontsize)
    plt.grid('on')
    plt.xlabel('Time $t$', fontsize=label_fontsize)
    plt.ylabel('$\|y_{u^*_{N}}(t) - \\tilde{{y}}^*_L(t)\|_{L^2(\Omega)}$', fontsize=label_fontsize)
    plt.tight_layout()
    plt.savefig(output_file)
    plt.show()
