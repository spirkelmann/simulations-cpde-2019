from __future__ import print_function
from fenics import *
import numpy as np

from plots import SimulationResult  # put this in here
from mpc import MPC
import scipy.io
import time

import os

# Set log level
set_log_level(WARNING)

class VelocityFieldExpression(Expression):
    def eval(self, value, x):
        value[0] = -1.0

    def value_shape(self):
        return (1,)

class Simulator:
    def __init__(self, n=100, sampling_rate=1e-2):
        # Prepare a mesh
        self.n = n
        mesh = UnitIntervalMesh(self.n)
        self.mesh = mesh
        self.U = FunctionSpace(mesh, "Lagrange", 1)
        self.W = VectorFunctionSpace(mesh, 'P', 1, dim=1)

        # Choose a time step size
        self.sampling_rate = sampling_rate
        self.dt = Constant(self.sampling_rate)

        # boundary heat conductivity parameters
        self.alpha = Constant(1.0)
        self.gamma_out = Constant(1.0e6)
        self.delta_out = Constant(1.0e6)
        self.gamma_c = Constant(0.0)
        self.delta_c = Constant(1.0e1)

        # Compile sub domains for boundaries
        left = CompiledSubDomain("near(x[0], 0.)")
        right = CompiledSubDomain("near(x[0], 1.)")

        # Label boundaries, required for the objective
        boundary_parts = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
        left.mark(boundary_parts, 0)    # left boundary part for outside temperature
        right.mark(boundary_parts, 1)   # right boundary part where control is applied
        self.ds = Measure("ds", subdomain_data=boundary_parts)

    def solve_forward(self, y0, us, ws, y_outs, record=False):
        """ The forward problem """
        ofile = File("results/y.pvd")

        # Define function spaces
        U = self.U
        W = self.W

        # Define test and trial functions
        v = TestFunction(U)
        y = TrialFunction(U)
        uu = Constant(1.0)
        yout = Constant(1.0)
        ww = Constant(1.0)

        e = VelocityFieldExpression(domain=self.mesh, degree=1)
        wfield = interpolate(e, W)

        alpha = self.alpha
        dt = self.dt
        gamma_c = self.gamma_c
        delta_c = self.delta_c
        gamma_out = self.gamma_out
        delta_out = self.delta_out
        ds = self.ds

        # Define variational formulation
        a = (y / dt * v + alpha * inner(grad(y), grad(v))) * dx + alpha * gamma_out * y * v * ds(0) \
            + alpha * gamma_c * y * v * ds(1) + ww * dot(wfield, grad(y)) * v * dx
        f_y = (y0 / dt * v ) * dx
        f_y_out = alpha * delta_out * yout * v * ds(0)
        f_u = alpha * delta_c * uu * v * ds(1)

        # Prepare solution
        y = Function(U, name="y")

        y_ol = [y0.vector().get_local()]

        L = len(us)
        for i in range(0,L):
            uu.assign(us[i])
            ww.assign(ws[i])   # we need a minus here, because in the optimization the w is on the
            yout.assign(y_outs[i])

            solve(a == f_u + f_y + f_y_out, y, solver_parameters={"linear_solver":"lu"})
            y0.assign(y)

            y_ol.append(y0.vector().get_local())

        return np.array(y_ol)

    def run_simulation(self, k0, y0, N=1, L=50, result_folder="results/", reference=False, tol=1e-9):

        sampling_rate = self.sampling_rate

        if reference:
            # compute reference solution
            N = L # horizon = full simulation window
            L = 1
        else:
            pass

        timer_start = time.time() # start timer

        mpc = MPC(N=N, result_folder=result_folder)

        U = self.U
        y = Function(U)
        yol = Function(U)
        yol_sim = Function(U)

        y.assign(y0)

        # closed loop data
        t_cl = [k0*self.sampling_rate]
        y_cl = [y.vector().get_local()]
        u_cl = []
        w_cl = []
        J_cl = []
        J_running = 0.0

        t_ol = []
        J_ol = []

        cpu_times = []

        for k in range(k0, k0+L):
            y_ol, u_ol, w_ol, cpu_time = mpc.solve(k, y, reference, tol)

            youts = []
            for j in range(0,N):
               youts.append(0.3 * sin(10 * sampling_rate * (k + j)))

            y_ol_sim = self.solve_forward(y, u_ol, w_ol, youts)

            t_ol.append(np.arange(k*sampling_rate, (k+N+1)*sampling_rate, sampling_rate))
            J_ol.append(0.5 * sampling_rate * sum(u_ol**2 + w_ol**2))

            y.vector().set_local(y_ol_sim[1])

            t_cl.append((k+1)*sampling_rate)
            y_cl.append(y.vector().get_local())
            u_cl.append(u_ol[0])
            w_cl.append(w_ol[0])
            J_running += sampling_rate * 0.5 * (u_ol[0]**2 + w_ol[0]**2)
            J_cl.append(J_running)

            cpu_times.append(cpu_time)

        timer_end = time.time() # stop timer
        time_total = float(timer_end - timer_start)

        # store closed-loop data
        np.savetxt(result_folder + 'sampling_rate.txt', np.ones(1) * sampling_rate)
        np.savetxt(result_folder + 'closedloop_t.txt', np.array(t_cl))
        np.savetxt(result_folder + 'closedloop_u.txt', np.array(u_cl))
        np.savetxt(result_folder + 'closedloop_w.txt', np.array(w_cl))
        np.savetxt(result_folder + 'closedloop_y.txt', np.array(y_cl))
        np.savetxt(result_folder + 'closedloop_cost.txt', np.array(J_cl))

        np.savetxt(result_folder + 'openloop_cost.txt', np.array(J_ol))
        np.savetxt(result_folder + 'total_time.txt', np.ones(1) * time_total)

        np.savetxt(result_folder + 'ipopt_cpu_times.txt', np.array(cpu_times))

        # load simulation result from stored data
        sim_result = SimulationResult(result_folder)

        return sim_result

    def output_matrices(self, output_folder="fenics_data"):
        # Define function space
        parameters.linear_algebra_backend = "Eigen"

        U = self.U
        W = self.W

        # Define test and trial functions
        v = TestFunction(U)
        y = TrialFunction(U)
        y0 = TrialFunction(U)

        u = Constant(1.0)
        y_out = Constant(1.0)

        w = Function(W)
        e = VelocityFieldExpression(domain=self.mesh, degree=1)
        w = interpolate(e, W)

        alpha = self.alpha
        dt = self.dt
        gamma_c = self.gamma_c
        delta_c = self.delta_c
        gamma_out = self.gamma_out
        delta_out = self.delta_out
        ds = self.ds

        # Define variational formulation
        a = (y / dt * v + alpha * inner(grad(y), grad(v))) * dx + alpha * gamma_out * y * v * ds(0) \
            + alpha * gamma_c * y * v * ds(1)
        f_w = dot(w, grad(y)) * v * dx
        f_y = y0 / dt * v * dx

        f_y_out = alpha * delta_out * y_out * v * ds(0)

        f_u = alpha * delta_c * u * v * ds(1)

        A = assemble(a)

        B_w = assemble(f_w)
        B_y = assemble(f_y)

        b_u = assemble(f_u)
        b_y_out = assemble(f_y_out)

        # make sure folder exists
        os.makedirs(os.path.dirname(output_folder + "/A.mtx"), exist_ok=True)

        # output matrices for use in optimization
        A_re = as_backend_type(A).sparray()
        scipy.io.mmwrite(output_folder + "/A.mtx", A_re, symmetry="general")

        B_w_re = as_backend_type(B_w).sparray()
        scipy.io.mmwrite(output_folder + "/B_w.mtx", B_w_re, symmetry="general")

        B_y_re = as_backend_type(B_y).sparray()
        scipy.io.mmwrite(output_folder + "/B_y.mtx", B_y_re, symmetry="general")

        b_u_re = b_u.array()
        b_u_file = open(output_folder + "/b_u.txt", "w")
        b_u_file.write(str(len(b_u_re)) + "\n")
        for val in b_u_re:
            b_u_file.write(str(val) + "\n")
        b_u_file.close()

        b_y_out_re = b_y_out.array()
        b_y_out_file = open(output_folder + "/b_y_out.txt", "w")
        b_y_out_file.write(str(len(b_y_out_re)) + "\n")
        for val in b_y_out_re:
            b_y_out_file.write(str(val) + "\n")
        b_y_out_file.close()

        h_file = open(output_folder + "/sampling_rate.txt", "w")
        h_file.write(str(self.sampling_rate) + "\n")
        h_file.close()

        param_file = open(output_folder + "/python_parameters.txt", "w")
        param_file.write(str((self.n)) + " n \n")
        param_file.write(str(float((self.alpha))) + " alpha \n")
        param_file.write(str(float((self.delta_c))) + " delta_c \n")
        param_file.write(str(float((self.delta_out))) + " delta_out \n")
        param_file.write(str(float((self.gamma_c))) + " gamma_c \n")
        param_file.write(str(float((self.gamma_out))) + " gamma_out \n")
        param_file.close()

        return b_u, b_y_out
