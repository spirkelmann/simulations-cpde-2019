from plots import *

from simulation import Simulator
from assumption_test import AssumptionTest

import matplotlib.colors as colors
import matplotlib.cm as cmx

linewidth_global = 2.0
label_fontsize = 30
legend_fontsize = 20
plt.rc('text', usetex=True)
plt.rc('font', family='serif')


if __name__ == "__main__":
    np.random.seed(42)
    simulate = True
    # set some general parameters
    n = 300 # number of finite elements
    sampling_rate = 1e-2    # sampling rate

    # convergence tests of optimal trajectory
    # 1. simulate optimal trajectories for different sampling rate h
    result_folder_global = 'convergence_test_1'

    window = 1.0    # time window

    # refine sampling rates
    refines = 4
    sampling_rates = [sampling_rate * 0.5**i for i in range(0, refines)]

    Ls = [int(window/s) for s in sampling_rates]

    for i in range(refines):
        sampling_rate_local = sampling_rates[i]

        # generate matrices from FEM discretization
        fenics_data_folder = "fenics_data"
        sim = Simulator(n, sampling_rate_local)
        sim.output_matrices(fenics_data_folder)

        ct = AssumptionTest(result_folder_global, n, sampling_rate_local)

        L = Ls[i]
        if simulate:
            ct.compute_optimal_trajectories([L])

    # load from file
    sim_result_ref = {}
    for L in Ls:
        result_folder_ref = result_folder_global + "/ref/L={}/".format(L)
        sim_result_ref[L] = SimulationResult(result_folder_ref)

    # plot L2 norm of y* for different sampling rates
    fig1 = plt.figure(1, figsize=(13, 8))
    for L in Ls:
        print("L = {}".format(L))
        h_ = 1.0 / L
        res = sim_result_ref[L]
        ts = np.arange(0.0, 1.0 + h_, h_)
        vs = np.array([np.linalg.norm(res.y_ol[0][j]) for j in range(0, L + 1)])
        plt.plot(ts, vs, label='$h = {}$'.format(h_))
        plt.ylim([0, 4])
    plt.grid('on')
    plt.ylabel("$\\|\\tilde{y}^*_L(t)\\|_{L_2(\\Omega)}$", fontsize=label_fontsize)
    plt.xlabel("Time $t$", fontsize=label_fontsize)
    plt.legend(loc='best', fontsize=legend_fontsize)
    plt.tight_layout()
    plt.savefig('figures/optimal_trajectory_convergence_1.pdf')

    # plot initial state of y* for different sampling rates
    nplt = int(np.ceil(np.sqrt(len(Ls))))
    fig2, axes = plt.subplots(nplt,nplt, figsize=(13, 8))
    i_fe = 1.0 / n
    domain = np.arange(0.0, 1.0 + 0.5 * i_fe, i_fe)
    for i in range(0,nplt):
        for j in range(0,nplt):
            if i*nplt+j < len(Ls):
                L = Ls[i*nplt+j]
                h_ = 1.0 / L
                print("L = {}".format(L))
                res = sim_result_ref[L]
                axes[i,j].plot(domain, res.y_ol[0][0])
                axes[i,j].set_xlabel('$\\Omega$', fontsize=label_fontsize)
                axes[i,j].set_ylabel('$\\tilde{y}^*_L(0)$', fontsize=label_fontsize)
                axes[i,j].set_title('$h = {}$'.format(h_), fontsize=label_fontsize)
                axes[i,j].grid('on')
    plt.tight_layout()
    plt.savefig('figures/optimal_trajectory_convergence_2.pdf')

    # 2. simulate optimal trajectories for different horizon length L
    result_folder_global = 'convergence_test_2'

    Ls = [100 * i for i in range(1, 6)]

    # generate matrices from FEM discretization
    fenics_data_folder = "fenics_data"
    sim = Simulator(n, sampling_rate)
    sim.output_matrices(fenics_data_folder)

    ct = AssumptionTest(result_folder_global, n, sampling_rate)

    if simulate:
        ct.compute_optimal_trajectories(Ls)

    # load from file
    sim_result_ref = {}
    for L in Ls:
        result_folder_ref = result_folder_global + "/ref/L={}/".format(L)
        sim_result_ref[L] = SimulationResult(result_folder_ref)

    # plot norm of y* for different horizon lengths
    fig3 = plt.figure(3, figsize=(13, 8))
    for L in Ls:
        print("L = {}".format(L))
        h_ = 1e-2
        res = sim_result_ref[L]
        ts = np.arange(0.0, L*h_ + h_, h_)
        vs = np.array([np.linalg.norm(res.y_ol[0][j]) for j in range(0, L + 1)])
        plt.plot(ts, vs, label='$L = {}$'.format(L))
        plt.ylim([0, 4])
    plt.grid('on')
    plt.ylabel("$\\|\\tilde{y}^*_L(t)\\|_{L_2(\\Omega)}$", fontsize=label_fontsize)
    plt.xlabel("Time $t$", fontsize=label_fontsize)
    plt.legend(loc='best', fontsize=legend_fontsize)
    plt.tight_layout()
    plt.savefig('figures/optimal_trajectory_convergence_3.pdf')

    # plot initial state y*(0) for different horizon lengths
    nplt = int(np.ceil(np.sqrt(len(Ls))))
    fig2, axes = plt.subplots(nplt, nplt, figsize=(13, 8))
    h = 1.0 / n
    domain = np.arange(0.0, 1.0 + 0.5 * h, h)
    for i in range(0, nplt):
        for j in range(0, nplt):
            if i * nplt + j < len(Ls):
                L = Ls[i * nplt + j]
                print("L = {}".format(L))
                res = sim_result_ref[L]
                axes[i, j].plot(domain, res.y_ol[0][0])
                axes[i, j].set_xlabel('$\\Omega$', fontsize=label_fontsize)
                axes[i, j].set_ylabel('$\\tilde{y}^*_L(0, \\cdot)$', fontsize=label_fontsize)
                axes[i, j].set_title('$L = {}$'.format(L), fontsize=label_fontsize)
                axes[i, j].grid('on')
    plt.tight_layout()
    plt.savefig('figures/optimal_trajectory_convergence_4.pdf')

    #####################
    # continuity tests  #
    #####################
    result_folder_global = 'continuity_test'
    # choose optimal trajectory for continuity tests
    L_ref = max(Ls)
    optimal_trajectory = sim_result_ref[L_ref]
    k = 50  # which time point?
    y_star = optimal_trajectory.y_ol[0][k]  # initial value on optimal trajectory y*

    # choose horizon lengths and disturbances
    Ns = [i * 10 for i in range(1, 11)]
    max_eps = 1e-2
    n_refinements = 5
    epsilons = [max_eps * 0.5 ** i for i in range(0, n_refinements)] + [0.0]
    n_dirs = 10

    if simulate:
        ct.result_folder_global = result_folder_global
        # simulate open loop trajectories for disturbed initial values
        ct.compute_open_loop_trajectories(k, y_star, Ns, epsilons, n_dirs)

    res_y_star_eps = {}
    # load open loop trajectories
    for i in range(n_dirs):
        for N in Ns:
            for eps in epsilons:
                result_folder = result_folder_global + "/N={}_i={}_eps={}/".format(N, i, eps)
                res_y_star_eps[(N, i, eps)] = SimulationResult(result_folder)

    results = {}
    for i in range(n_dirs):
        for N in Ns:
            for eps in epsilons:
                results[(N, i, eps)] = np.abs(res_y_star_eps[(N, i, eps)].open_loop_cost - res_y_star_eps[(N, i, 0.0)].open_loop_cost)

    results_max = {}
    for N in Ns:
        for eps in epsilons:
            subkeys = filter(lambda key: key[0] == N and key[2] == eps, results.keys())
            subresults = [results[key] for key in subkeys]

            results_max[(N, eps)] = np.max(subresults)

    # 1. plot optimal value functions for different epsilons
    plt.figure(5, figsize=(13, 8))
    plt.grid('on')
    plt.xlabel('$\\varepsilon$', fontsize=label_fontsize)
    plt.ylabel('$\\tilde{{\\delta}}_{{{}}}(N, \\varepsilon)$'.format(k),fontsize=label_fontsize)
    color_vals = range(len(Ns))
    jet = cm = plt.get_cmap('gist_rainbow')
    cNorm = colors.Normalize(vmin=0, vmax=color_vals[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    i = 0
    Ns_plot = Ns[0:-1:2]
    for N in Ns_plot:
        eps_keys = map(lambda key: key[1], results_max.keys())
        eps_keys_sort = sorted(eps_keys)

        values = [results_max[(N, eps_key)] for eps_key in eps_keys_sort]

        colorVal = scalarMap.to_rgba(color_vals[i])
        plt.plot(eps_keys_sort, values, marker='+', label='$N={}$'.format(N), linewidth=linewidth_global)#, color=colorVal)

        i += 1

    plt.tick_params(axis='both', which='major', labelsize=label_fontsize)
    plt.legend(loc='best', fontsize=legend_fontsize)
    plt.tight_layout()
    plt.savefig('figures/continuity_simulation_1.pdf')

    # plot optimal value functions for different horizon lengths
    plt.figure(6, figsize=(13, 8))
    plt.grid('on')
    plt.xlabel('$N$', fontsize=label_fontsize)
    plt.ylabel('$\\tilde{{\\delta}}_{{{}}}(N, \\varepsilon)$'.format(k), fontsize=label_fontsize)
    color_vals = range(len(epsilons))
    jet = cm = plt.get_cmap('gist_rainbow')
    cNorm = colors.Normalize(vmin=0, vmax=color_vals[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    i = 0
    # plot over Ns
    for eps in epsilons:
        N_keys = map(lambda key: key[0], results_max.keys())
        N_keys_sort = sorted(N_keys)
        values = [results_max[(N_key, eps)] for N_key in N_keys_sort]

        colorVal = scalarMap.to_rgba(color_vals[i])
        plt.plot(N_keys_sort, values, marker='+', label='$\\varepsilon={}$'.format(eps), linewidth=linewidth_global)#, color=colorVal)

        i += 1

    plt.xlim([min(Ns), max(Ns)])
    plt.tick_params(axis='both', which='major', labelsize=label_fontsize)
    plt.legend(loc='best', fontsize=legend_fontsize)
    plt.tight_layout()
    plt.savefig('figures/continuity_simulation_2.pdf')
    plt.show()

    ###################
    # turnpike tests  #
    ###################
    result_folder_global = 'turnpike_test'

    # choose horizon lengths and disturbances
    Ns = [i * 20 for i in range(1, 6)]
    n_ys = 4
    ys = [0.05 * i for i in range(1, n_ys)] + [0.0] + [-0.05 * i for i in range(1, n_ys)]

    k = 40
    y_star = np.zeros(n+1)
    if simulate:
        ct.result_folder_global = result_folder_global
        ct.compute_turnpike_trajectories_Ns(k, y_star, Ns)
        ct.compute_turnpike_trajectories_ys(k, ys, max(Ns))

    res_turnpike_N = []
    # load open loop trajectories
    for N in Ns:
        result_folder = result_folder_global + "/N={}/".format(N)
        res_turnpike_N.append(SimulationResult(result_folder))

    res_turnpike_y = []
    # load open loop trajectories
    for y in ys:
        result_folder = result_folder_global + "/N={}_y={}/".format(max(Ns), y)
        res_turnpike_y.append(SimulationResult(result_folder))

    plot_turnpikes_Ns(res_turnpike_N, optimal_trajectory, k=k, output_file='figures/turnpike_simulation_1.pdf')
    plot_turnpike_ys(res_turnpike_y, optimal_trajectory, k=k, output_file='figures/turnpike_simulation_2.pdf')


    print("done")