/*
 * hs071_nlp.hpp
 *
 *  Created on: Dec 3, 2017
 *      Author: martin
 */

#ifndef MATRIXOP_HPP_
#define MATRIXOP_HPP_


#include "boost/filesystem.hpp"

#include <valarray>
#include <string>
#include <fstream>
#include <cassert>
#include <iostream>
#include <ctime>


using namespace std;

class MATRIXOP {
public:

    MATRIXOP();
    MATRIXOP(int N_, string data_folder, double eps_,
             double y_ref_, double u_ref_, double u_upper_, double u_lower_, double y_upper_, double y_lower_,
             double w_upper_, double w_lower_, bool convection, bool open_values, bool free_init_value,
             string result_folder, string result_folder_name, int k, string file_y0);


    /** Default destructor */
    ~MATRIXOP();

    void read_matrix(string filename, valarray<int> &rows, valarray<int> &cols, valarray<double> &vals);
    void read_vector(string filename, valarray<double> &vals);
    void read_value(string filename, double &val );
    void print_matrix(valarray<int> &rows, valarray<int> &cols, valarray<double> &vals) const;
    void print_vector(valarray<double> &vals) const;
    void create_folder(string folder_prefix);

    double eval_f(valarray<double> &x);
    double vec_Q_vec(valarray<double> y, double y_ref);
    double vec_R_vec(valarray<double> y, double y_ref);
    double vec_W_vec(valarray<double> y);

    valarray<double> eval_grad_f(valarray<double> z);
    valarray<double> Q_vec(valarray<double> y);
    valarray<double> R_vec(valarray<double> u);
    valarray<double> W_vec(valarray<double> w);

    void A_eq();
    valarray<double> matrix_vector_mult(valarray<int> &rows,
            valarray<int> &cols, valarray<double> &vals, valarray<double> &vec);

    valarray<int> A_rows, A_cols, B_y_rows, B_y_cols, A_eq_rows, A_eq_cols, B_w_rows, B_w_cols, order;
    valarray<double> A_vals, B_y_vals, A_eq_vals, B_w_vals, b_u, b_y;
    valarray<double> y0;
    int n_y, n_u, n_w, n_z, N;
    double eps, y_ref, u_ref, u_upper, u_lower, y_upper, y_lower, w_upper, w_lower, sampling_rate;

    int k; // current time step

    bool convection, open_values, free_init_value;
    string foldername; // folder where results shall be stored
    string result_folder; // folder where the above folder shall be placed
private:


};





#endif /* MATRIXOP_HPP_ */
