%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[
12pt, % Default font size is 10pt, can alternatively be 11pt or 12pt
a4paper, % Alternatively letterpaper for US letter
onecolumn, % Alternatively twocolumn
portrait % Alternatively landscape
]{article}

\input{structure.tex} % Input the file specifying the document layout and structure

%----------------------------------------------------------------------------------------
%	ARTICLE INFORMATION
%----------------------------------------------------------------------------------------

\articletitle{PDE optimization of the convection-diffusion equation with time-varying boundary conditions} 

\datenotesstarted{December 4, 2017}
\docdate{\datenotesstarted; rev. \today}

\docauthor{Simon Pirkelmann}

%----------------------------------------------------------------------------------------

\begin{document}

\pagestyle{myheadings} % Use custom headers
\markright{\doctitle} % Place the article information into the header

%----------------------------------------------------------------------------------------
%	PRINT ARTICLE INFORMATION
%----------------------------------------------------------------------------------------

\thispagestyle{plain} % Plain formatting on the first page

\printtitle % Print the title

%----------------------------------------------------------------------------------------
%	ARTICLE NOTES
%----------------------------------------------------------------------------------------
This document describes the numerical implementation of the problem in the paper. First the setting is introduced. The next section deals with the numerical forward simulation of PDE using the finite element method provided by \texttt{FEniCs}. Afterwards we describe how the discretized optimal control problem is formulated and implemented in \texttt{C++} using the solver \texttt{Ipopt}. We conclude with an example that lists the parameters used for the simulations.
\section{Setting}
Consider the following equation
\begin{equation}
y_t -  \alpha \Delta y + w \nabla y = 0 \text{ on } Q := \Omega \times [0,T]
\label{eq:pde}
\end{equation}
where $y : Q \rightarrow \mathbb{R}$ is the temperature, $\alpha \in \mathbb{R}$ is the diffusion coefficient and $w : [0,T] \rightarrow \Omega$ is the velocity field. We use the shorthand $y_t = \frac{\partial y}{\partial t}$ to denote the time derivative.

Let $\Omega$ be the domain. The boundary is partitioned in an boundary $\Gamma_{out}$ where some outside temperature is prescribed and a control boundary $\Gamma_c$. On one part of the boundary a controllable t is applied which is described by a Dirichlet condition
\begin{equation}
y = u \text{ on } \Gamma_c.
\end{equation}
On the other part we have
\begin{equation}
y = y_{out} \text{ on } \Gamma_{out}
\end{equation}
where $\frac{\partial y}{\partial n}$ is the derivative of $y$ in normal direction.
We approximate the Dirichlet boundary conditions by using the following Robin type boundary condition instead
\begin{equation}
\frac{\partial y}{\partial n} + \gamma y = \delta z \text{ on } \Gamma.
\label{eq:robin-bc}
\end{equation}
By choosing 
$\gamma := \begin{cases}
10^6 =: \gamma_{out} &\text{ on } \Gamma_{out} \\ 
0 =: \gamma_{c} &\text{ on } \Gamma_c \\
\end{cases}$, $\delta := \begin{cases}
10^6 =: \delta_{out} &\text{ on } \Gamma_{out} \\ 
10 =: \delta_c &\text{ on } \Gamma_c \\
\end{cases}$
and
$z := \begin{cases}
y_{out} &\text{ on } \Gamma_{out} \\ 
u &\text{ on } \Gamma_c \\
\end{cases}
$
we can approximate the Dirichlet boundary conditions. This will also allow us to extend the setting more easily in the future.
\section{Numerical simulation of the heat equation}
We simulate the equation using the finite element method.
\subsection{Weak form}
For the weak form we first discretize in time by picking a sampling rate $\Delta t > 0$. We define $y_k := y(\cdot, t_0 + \Delta t k)$, $y_{out,k} := y_{out}(\cdot, t_0 + \Delta t k) $, $u_k := u(t_0 + \Delta t k)$, $z_k := z(t_0 + \Delta t k)$ for $k \in \{0, 1, \hdots, N\}$. \\
The initial value $y_0$ is given. To compute the next state $y_{k+1}$ for each $k \in \{0, 1, \hdots, N-1\}$  we replace $y_t$ in equation \eqref{eq:pde} by $\frac{y_{k+1} - y_k}{\Delta t}$ and $y$ by $y_{k+1}$ using backward Euler. Multiplying with a test function $v$ and integrating over the domain $\Omega$ yields
\begin{equation}
\int_{\Omega} \frac{y_{k+1} - y_k}{\Delta t} v \; dx - \alpha \int_{\Omega} \Delta y_{k+1} v \; dx + \int_{\Omega} w_{k} \nabla y_{k+1} v \; dx = 0 \text{ for } k \in \{0, 1, \hdots, N-1\}.
\end{equation}
Using partial integration on the second integral we get
\begin{equation}
\int_{\Omega} \frac{y_{k+1} - y_k}{\Delta t} v \; dx - \alpha \int_{\Gamma}  \frac{\partial y_{k+1}}{\partial n} v \; ds + \alpha \int_{\Omega} \nabla y_{k+1} \cdot \nabla v \; dx + \int_{\Omega} w_{k} \nabla y_{k+1} v \; dx = 0
\end{equation}
Substituting the boundary condition \eqref{eq:robin-bc} we obtain
\begin{equation}
\int_{\Omega} \frac{y_{k+1} - y_k}{\Delta t} v \; dx + \alpha \int_{\Gamma} (\gamma z_k - \delta y_{k+1}) v \; ds + \alpha \int_{\Omega} \nabla y_{k+1} \cdot \nabla v \; dx + \int_{\Omega} w_{k} \nabla y_{k+1} v \; dx = 0
\end{equation}
TODO: Strictly speaking, for backward Euler we may actually have to use $z_{k+1}$ and $w_{k+1}$ here instead. Think about this!\\
Substituting the definition of $z_k$ on the different parts $\Gamma_{out}$ and $\Gamma_c$ of the boundary we get
\begin{equation}
\begin{aligned}
\int_{\Omega} \frac{y_{k+1} - y_k}{\Delta t} v \; dx +  \alpha \int_{\Gamma_c} (\gamma_c u_k - \delta_c y_{k+1}) v \; ds  +  \alpha\int_{\Gamma_{out}} (\gamma_{out} y_{out,k} - \delta_{out} y_{k+1}) v \; ds \\
+ \alpha \int_{\Omega} \nabla y_{k+1} \cdot \nabla v \; dx + \int_{\Omega} w_{k} \nabla y_{k+1} v \; dx = 0
\label{eq:fem-state-equation}
\end{aligned}
\end{equation}

\subsection{Implementation in \texttt{FEniCS}}
The above variational equation is implemented in \texttt{Python} with \texttt{FEniCS} to obtain the system matrices for use in the optimization.

Reordering the terms in \eqref{eq:fem-state-equation} by terms depending on the state $y_{k+1}$ and all external terms we get:
\begin{equation}
\begin{aligned}
\underbrace{\int_{\Omega} \frac{y_{k+1}}{\Delta t} v + \alpha \nabla y_{k+1} \cdot \nabla v \; dx + \alpha \int_{\Gamma} \gamma  y_{k+1} v \; ds}_{a} + \underbrace{\int_{\Omega} w_{k} \nabla y_{k+1} v \; dx}_{f_w} \\
= \underbrace{\int_{\Omega} \frac{y_k}{\Delta t} v \; dx}_{f_y} + \underbrace{\alpha \int_{\Gamma_c} \delta_c  u_k v \; ds}_{f_u}  +  \underbrace{\alpha\int_{\Gamma_{out}} \delta_{out}  y_{out,k} v \; ds}_{f_{y,out}}
\label{eq:weak-form-reordered}
\end{aligned}
\end{equation}
This is a linear system of equations which we can explicitely assemble in \texttt{FEniCS} using the \texttt{assemble()} function. We obtain the matrices and vectors
\begin{align*}
A \in \mathbb{R}^{n_y \times n_y}, B_w \in \mathbb{R}^{n_y \times n_y} B_y \in \mathbb{R}^{n_y \times n_y}, b_u \in \mathbb{R}^{n_y}, b_{y,out} \in \mathbb{R}^{n_y},
\end{align*}
where $n_y$ is the number of finite elements used in the spatial discretization.
Equation \eqref{eq:weak-form-reordered} then corresponds to the linear system
\begin{equation}
A y_{h,k+1} + w_k B_w y_{h,k+1} = B_y y_{h,k} + b_u u_k + b_{y,out} y_{out,k},
\end{equation}
making use of the fact that $u_k$ and $y_{out,k}$ are constant on the boundary. The system matrices and vectors are generated in \texttt{FEniCS} and then saved to a file for subsequent use.

\begin{python}
class VelocityFieldExpression(Expression):
    def eval(self, value, x):
        value[0] = -1.0

    def value_shape(self):
        return (1,)

class Simulator:
    def __init__(self, n=100, sampling_rate=1e-2):
        # Prepare a mesh
        self.n = n
        mesh = UnitIntervalMesh(self.n)
        self.mesh = mesh
        self.U = FunctionSpace(mesh, "Lagrange", 1)
        self.W = VectorFunctionSpace(mesh, 'P', 1, dim=1)

        # Choose a time step size
        self.sampling_rate = sampling_rate
        self.dt = Constant(self.sampling_rate)

        # boundary heat conductivity parameters
        self.alpha = Constant(1.0)
        self.gamma_out = Constant(1.0e6)
        self.delta_out = Constant(1.0e6)
        self.gamma_c = Constant(0.0)
        self.delta_c = Constant(1.0e1)

        # Compile sub domains for boundaries
        left = CompiledSubDomain("near(x[0], 0.)")
        right = CompiledSubDomain("near(x[0], 1.)")

        # Label boundaries, required for the objective
        boundary_parts = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
        left.mark(boundary_parts, 0)    # left boundary part for outside temperature
        right.mark(boundary_parts, 1)   # right boundary part where control is applied
        self.ds = Measure("ds", subdomain_data=boundary_parts)
        
    def output_matrices(self, output_folder="fenics_data"):
        # Define function space
        parameters.linear_algebra_backend = "Eigen"

        U = self.U
        W = self.W

        # Define test and trial functions
        v = TestFunction(U)
        y = TrialFunction(U)
        y0 = TrialFunction(U)

        u = Constant(1.0)
        y_out = Constant(1.0)

        w = Function(W)
        e = VelocityFieldExpression(domain=self.mesh, degree=1)
        w = interpolate(e, W)

        alpha = self.alpha
        dt = self.dt
        gamma_c = self.gamma_c
        delta_c = self.delta_c
        gamma_out = self.gamma_out
        delta_out = self.delta_out
        ds = self.ds

        # Define variational formulation
        a = (y / dt * v + alpha * inner(grad(y), grad(v))) * dx + alpha * gamma_out * y * v * ds(0) \
            + alpha * gamma_c * y * v * ds(1)
        f_w = dot(w, grad(y)) * v * dx
        f_y = y0 / dt * v * dx

        f_y_out = alpha * delta_out * y_out * v * ds(0)

        f_u = alpha * delta_c * u * v * ds(1)

        A = assemble(a)

        B_w = assemble(f_w)
        B_y = assemble(f_y)

        b_u = assemble(f_u)
        b_y_out = assemble(f_y_out)

        # make sure folder exists
        os.makedirs(os.path.dirname(output_folder + "/A.mtx"), exist_ok=True)

        # output matrices for use in optimization
        A_re = as_backend_type(A).sparray()
        scipy.io.mmwrite(output_folder + "/A.mtx", A_re, symmetry="general")

        B_w_re = as_backend_type(B_w).sparray()
        scipy.io.mmwrite(output_folder + "/B_w.mtx", B_w_re, symmetry="general")

        B_y_re = as_backend_type(B_y).sparray()
        scipy.io.mmwrite(output_folder + "/B_y.mtx", B_y_re, symmetry="general")

        b_u_re = b_u.array()
        b_u_file = open(output_folder + "/b_u.txt", "w")
        b_u_file.write(str(len(b_u_re)) + "\n")
        for val in b_u_re:
            b_u_file.write(str(val) + "\n")
        b_u_file.close()

        b_y_out_re = b_y_out.array()
        b_y_out_file = open(output_folder + "/b_y_out.txt", "w")
        b_y_out_file.write(str(len(b_y_out_re)) + "\n")
        for val in b_y_out_re:
            b_y_out_file.write(str(val) + "\n")
        b_y_out_file.close()

        h_file = open(output_folder + "/sampling_rate.txt", "w")
        h_file.write(str(self.sampling_rate) + "\n")
        h_file.close()

        param_file = open(output_folder + "/python_parameters.txt", "w")
        param_file.write(str((self.n)) + " n \n")
        param_file.write(str(float((self.alpha))) + " alpha \n")
        param_file.write(str(float((self.delta_c))) + " delta_c \n")
        param_file.write(str(float((self.delta_out))) + " delta_out \n")
        param_file.write(str(float((self.gamma_c))) + " gamma_c \n")
        param_file.write(str(float((self.gamma_out))) + " gamma_out \n")
        param_file.close()

        return b_u, b_y_out
\end{python}

\section{Optimal Control Problem}
Our goal is to solve the following problem:
\begin{align*}
\min_{y,u, w} J(y, u, w) = & \frac{\varepsilon}{2}\int_{\Omega} (y(x, T) - y_{\Omega}(T,x))^2 \; dx + \frac{\varepsilon}{2} \int_{0}^{T} \int_{\Omega} (y(x,t) - y_{\Omega}(t,x))^2 \; dx \; dt \\
& + \frac{1}{2} \int_{0}^{T} \int_{\Gamma_c} (u(x,t) - u_{ref}(x,t))^2 \; ds \; dt + \frac{1}{2} \int_{0}^{T} w(t)^2 \; dt \\
\text{s.t.} & \eqref{eq:pde}, \eqref{eq:robin-bc} \\
& \underline{u}(x,t) \leq u(x,t) \leq \overline{u}(x,t) \\
& \underline{y}(x,t) \leq y(x,t) \leq \overline{y}(x,t) \text{ on } \Omega_y
\end{align*}
where $\Omega_y \subset \Omega$.
\\
The problem is solved by the First-Discretize-Then-Optimize-Approach.
We replace $y$ by its finite element approximations $y_{h,k}$ for each time point $t = k \Delta t, k \in \{0, \hdots, N\}$. Let $y_h = (y_{h,0}, \hdots, y_{h,N})$, $u = (u_0, \hdots, u_{N-1})$, $w = (w_0, \hdots, w_{N-1})$ and $y_{out} = (y_{out,0}, \hdots, y_{out,N-1})$.
We end up with a finite dimensional optimal control problem which reads:
\begin{align*}
& \min_{y_h,u, w} J(y_h, u, w) = \sum_{k=0}^{N-1} \left( \frac{\varepsilon}{2} (y_{h,k} - y_{\Omega,k})^T Q (y_{h,k} - y_{\Omega,k}) + \frac{1}{2} (u_k - u_{ref,k})^T R (u_k - u_{ref,k}) \right. \\  & \left. + \frac{1}{2} w_k^T W w_k \right) + \frac{\varepsilon}{2} (y_{h,N} - y_{\Omega,N})^T Q (y_{h,N} - y_{\Omega,N}) \\
\text{s.t. }  & A y_{h,k+1} + w_k B_w y_{h,k+1} = B_y y_{h,k} + b_u u_k + b_{y,out} y_{out,k} \text{ for } k \in \{0, \hdots, N-1\} \\
&\underline{y}_{h,k,i} \leq y_{h,k,i} \leq \overline{y}_{h,k,i} \text{ for } k \in \{0, \hdots, N\}, i \in \mathcal{I}_{\Omega_y} \\
&\underline{u}_{k} \leq u_{k} \leq \overline{u}_{k} \text{ for } k \in \{0, \hdots, N-1\}
\end{align*}
where $\mathcal{I}_{\Omega_y}$ is the set of all indices with finite element nodes within the set $\Omega_y$. When using Lagrange finite elements the degrees of freedom of the FE approximation $y_h$ of the state $y$ correspond to the value of $y_h$ at the finite element nodes. This means we can enforce the state constraint pointwise at the finite element nodes, simply by constraining the corresponding elements of $y_h$. \\

In the following it is described how the open-loop optimal control problems are solved in C++ with Ipopt. 
\def\eQ{\varepsilon Q}
\subsection{Implementation in C++}
In the Ipopt implementation the optimization variable is $z = (y_h, u, w) \in \mathbb{R}^{n_z}$, where $n_z = (N+1)n_y + N n_u  + N n_w$. The function for the nonlinear constraints is given by
\begin{equation}
g(z) =
\begin{pmatrix}
A y_{h,1} + w_0 B_w y_{h,1} - B_y y_{h,0} - b_u u_0 - b_{y,out} y_{out,0} \\
\vdots \\
A y_{h,N} + w_{N-1} B_w y_{h,N} - B_y y_{h,N-1} - b_u u_{N-1} - b_{y,out} y_{out,N-1}
\end{pmatrix} \in \mathbb{R}^{N n_y}.
\label{eq:ipopt-nonlinear-system-constraints}
\end{equation}
The Jacobian of $g$ is given by
\begin{equation}
\scriptsize
\nabla g (z) =
\underbrace{\begin{pmatrix}
-B_y & A + w_0 B_w &             &        &                 & -b_u &      &        &      & B_w y_{h,1} &             &       &  \\
     &        -B_y & A + w_1 B_w &        &                 &      & -b_u &        &      &             & B_w y_{h,2} &       &  \\
     &             &             & \ddots &                 &      &      & \ddots &      &             &             &\ddots &  \\
     &             &             &   -B_y & A + w_{N-1} B_w &      &      &        & -b_u &             &             &       & B_w y_{h,N} 
\end{pmatrix}}_{
\in \mathbb{R}^{N n_y \times n_z}}
\end{equation}
The gradient of the objective function is given by
\begin{equation}
\nabla J(z) = 
\begin{pmatrix}
\varepsilon Q (y_{h,0} - y_{\Omega,0}) \\
\vdots \\
\varepsilon Q (y_{h,N} - y_{\Omega,N}) \\
R (u_0 - u_{ref,0}) \\
\vdots \\
R (u_{N-1} - u_{ref,N-1}) \\
W w_0 \\
\vdots \\
W w_{N-1} \\
\end{pmatrix} \in \mathbb{R}^{n_z},
\end{equation}
and the Hessian of the objective function by
\begin{equation}
\nabla^2 J(z) = 
\begin{pmatrix}
\eQ &        &     &   &        &   &   &        &   &                  \\
    & \ddots &     &   &        &   &   &        &   &                  \\
    &        & \eQ &   &        &   &   &        &   &                  \\
    &        &     & R &        &   &   &        &   &                  \\
    &        &     &   & \ddots &   &   &        &   &                  \\
    &        &     &   &        & R &   &        &   &                  \\
    &        &     &   &        &   & W &        &   &                  \\
    &        &     &   &        &   &   & \ddots &   &                  \\
    &        &     &   &        &   &   &        & W &                  \\
\end{pmatrix} \in \mathbb{R}^{n_z \times n_z}.
\end{equation}

For the Hessian of the Lagrange function we additionally need to specify the matrices $\nabla^2 g_i(z)$. Those are of the form 
\def\Bw{B_w(j,:)}
\def\Bwt{\Bw^T}
\begin{scriptsize}
\begin{equation}
\nabla^2 g_k(z) = 
\bordermatrix{
              & y_0 & \hdots & y_{i+1} & \hdots & y_N & u_0 & \hdots & u_{N-1} & w_0 & \hdots & w_i & \hdots & w_{N-1} \cr
y_0           &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
\vdots        &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
y_{i+1}       &     &        &         &        &     &     &        &         &     &        & \Bwt&        &         \cr
\vdots        &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
y_N           &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
u_0           &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
\vdots        &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
u_{N-1}       &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
w_0           &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
\vdots        &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
w_i           &     &        &    \Bw  &        &     &     &        &         &     &        &     &        &         \cr
\vdots        &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
w_{N-1}       &     &        &         &        &     &     &        &         &     &        &     &        &         \cr
}
\end{equation}
\end{scriptsize}
for $k \in \{1, \hdots, N n_y\}$ where $\Bw$ denotes the $j-$th row of the matrix $B_w$, i.e.
we have 
\begin{equation}
\nabla^2 g_{k}(z)_{(N+1)n_y+N n_U+i+1,(i+1) n_y + l} = B_w(j,l)
\end{equation}
if $k = i n_y + j$ with $i \in \{0, \hdots, N-1\}$ and $j,l \in \{1, \hdots, n_y\}$.
The Hessian of the Lagrange functional is then given by
\begin{equation}
\sigma_f \nabla^2 J(z) + \sum_{k=1}^{N n_y} \lambda_k \nabla^2 g_k(z)
\end{equation}
see also \url{https://www.coin-or.org/Ipopt/documentation/node22.html}.

NOTE: When the convection term is present, the Jacobian and Hessian matrices are no longer constant!

NOTE: Before implementing the exact Hessian of the Lagrangian we should try using the Hessian of the objective function as an approximation and the approximation by a quasi-Newton method like BFGS (set the option \texttt{hessian\_approximation} to \texttt{limited-memory} for this, see \url{https://www.coin-or.org/Ipopt/documentation/node31.html}).

\section{Example from the paper}
The parameters from the following example were used in the simulations for the paper. Let $\Omega$ be the unit interval $[0,1]$ and let $\Omega_y := [\frac{1}{4},\frac{3}{4}]$ and let $\Gamma_{out}$ be the left boundary (for $x=0$) and $\Gamma_c$ be the right boundary (for $x=1$). The domains and subdomains are illustrated in Figure \ref{fig:domain}.
\begin{figure}[h]
\begin{center}
\begin{tikzpicture}

\draw [black] plot coordinates {(0,-0.1) (0,0.1)};
\draw [black] plot coordinates {(4,-0.1) (4,0.1)};


\draw [black] plot coordinates { (0,0) (1,0)};
\draw [orange] plot coordinates { (1,0) (3,0)};
\draw [black] plot coordinates { (3,0) (4,0)};

\draw	
		(2, -0.35) node[orange] {\large{$\Omega_y$}}
		(2, 0.5) node {\large{$\Omega$}}
		(4, -0.4) node[red] {\large{$\Gamma_c$}}
		(0, -0.4) node[blue] {\large{$\Gamma_{out}$}}
		;
\end{tikzpicture}
\end{center}
\caption{Illustration of the domain.}
\label{fig:domain}
\end{figure}

The mesh is discretized by $n_y = 300$ finite elements. The dimension of the controls is $n_u = 1$, $n_w = 1$. We pick a sampling rate $h = 10^{-2}$. At the boundary $\Gamma_{out}$ we have the time-varying boundary value function 
\begin{equation}
y_{out}(t) = \frac{1}{2} + \frac{1}{3} \sin(10t).
\end{equation}
For discrete time points $t = k h$ this corresponds to
\begin{equation}
y_{out,k} = \frac{1}{2} + \frac{1}{3} \sin(\frac{k}{10}).
\end{equation}
We choose the parameters of the PDE system
\begin{align*}
\alpha &= 1 \\
\gamma_{out} &= 10^{6} \\
\delta_{out} &= 10^6 \\
\gamma_c &= 0 \\
\delta_c &= 10
\end{align*}
and parameters for the optimal control problem
\begin{align*}
\varepsilon &= 0 \\
Q &= I_{n_y \times n_y} \\
R &= I_{n_u \times n_u} = 1\\
W &= I_{n_w \times n_w} = 1\\
N &= 10\\
y_{\Omega,k} &= 0.5 \text{ for } k \in \{0, \hdots, N\} \\
u_{ref,k} &= 0.5 \text{ for } k \in \{0, \hdots, N-1\}.
\end{align*}
The constraints for control and state are given by
\begin{align*}
\overline{y}_{h,k,i} &= - \underline{y}_{h,k,i} = 0.15  \text{ for } k \in \{0, \hdots, N\}, i \in \mathcal{I}_{\Omega_y}\\
\overline{y}_{h,k,i} &= -\underline{y}_{h,k,i} = 10 \text{ for } k  \in \{0, \hdots, N\}, i \not\in \mathcal{I}_{\Omega_y}\\
\overline{u}_k &= -\underline{u}_k = 0.25 \text{ for } k \in \{0, \hdots, N-1\}
\end{align*}
In the case that we use Langrange finite elements we have that $\mathcal{I}_{\Omega_y} = \{\frac{n_y}{4}, \hdots, \frac{3 n_y}{4}\}$.
\newpage
%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\renewcommand{\refname}{Reference} % Change the default bibliography title

\nocite{wachteripopt}
\nocite{logg2012automated}
\bibliography{literature} % Input your bibliography file
\bibliographystyle{plain}


%----------------------------------------------------------------------------------------

\end{document}
