import multiprocessing
from fenics import *

from simulation import Simulator

import numpy as np

class AssumptionTest:
    def __init__(self, result_folder_global, n=100, sampling_rate=1e-2):
        self.result_folder_global = result_folder_global

        self.n = n
        self.sampling_rate = sampling_rate

        self.directions = {}
        self.k = None
        self.y_star = None

        self.y_inits = {}

    def compute_optimal_trajectory(self, L):
        print("L = {}".format(L))
        sim = Simulator(self.n, self.sampling_rate)

        result_folder = self.result_folder_global + "/ref/L={}/".format(L)
        y0 = interpolate(Expression("0.0", degree=1), sim.U)
        sim.run_simulation(0, y0, L=L, result_folder=result_folder, reference=True, tol=1e-12)

    def compute_optimal_trajectories(self, Ls):
        num_cores = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(processes=num_cores)
        pool.map(self.compute_optimal_trajectory, Ls)

    def compute_open_loop_trajectory(self, input):
        print("(N, eps, dir) = {}".format(input))
        (N, eps, direction_index) = input
        direction = self.directions[direction_index]
        result_folder = self.result_folder_global + "/N={}_i={}_eps={}/".format(N, direction_index, eps)

        sim = Simulator(self.n, self.sampling_rate)
        U = sim.U
        y0 = Function(U)
        y_star_eps = self.y_star + direction * eps
        y0.vector().set_local(np.flipud(y_star_eps))

        sim.run_simulation(self.k, y0, N=N, L=1, result_folder=result_folder, tol=1e-9)

    def compute_open_loop_trajectories(self, k, y_star, Ns, epsilons, n_dirs):
        # generate directions
        self.generate_directions(n_dirs)
        direction_indices = self.directions.keys()

        self.k = k
        self.y_star = y_star

        params = []
        for N in Ns:
            for eps in epsilons:
                for d in direction_indices:
                    params.append((N, eps, d))

        num_cores = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(processes=num_cores)
        pool.map(self.compute_open_loop_trajectory, params)

    def generate_directions(self, n_dirs):
        sim = Simulator(self.n, self.sampling_rate)
        # generate directions
        U = sim.U
        y = Function(U)
        for i in range(n_dirs):
            # generate random disturbance
            delta = np.random.rand(self.n + 1) - 0.5
            delta[0] = 0.0
            y.vector().set_local(delta)
            y.vector().set_local(y.vector().get_local() / norm(y))  # normalize disturbance
            delta = y.vector().get_local()
            self.directions[i] = delta


    def compute_open_loop_trajectory_turnpike_N(self, input):
        print("N = {}".format(input))
        N = input
        y = self.y_star
        result_folder = self.result_folder_global + "/N={}/".format(N)

        sim = Simulator(self.n, self.sampling_rate)
        U = sim.U
        y0 = Function(U)
        y0.vector().set_local(np.flipud(y))

        sim.run_simulation(self.k, y0, N=N, L=1, result_folder=result_folder, tol=1e-9)

    def compute_open_loop_trajectory_turnpike_y(self, input):
        print("(N, y) = {}".format(input))
        (N,y) = input
        result_folder = self.result_folder_global + "/N={}_y={}/".format(N, y)

        sim = Simulator(self.n, self.sampling_rate)
        U = sim.U
        y0 = Function(U)
        y = y * np.ones(self.n+1)
        y0.vector().set_local(np.flipud(y))

        sim.run_simulation(self.k, y0, N=N, L=1, result_folder=result_folder, tol=1e-9)

    def compute_turnpike_trajectories_Ns(self, k, y, Ns):
        self.k = k
        self.y_star = y

        params = []
        for N in Ns:
            params.append(N)

        num_cores = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(processes=num_cores)
        pool.map(self.compute_open_loop_trajectory_turnpike_N, params)

    def compute_turnpike_trajectories_ys(self, k, ys, N):
        self.k = k

        params = []
        for y in ys:
            params.append((N, y))

        num_cores = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(processes=num_cores)
        pool.map(self.compute_open_loop_trajectory_turnpike_y, params)