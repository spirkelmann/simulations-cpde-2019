Readme for the Python/C++ version of the code
---------------------------------------------
Requirements:
- Python version: 3.5.2
- FEniCS version: 2017.2.0
- gcc version: 5.4.0
- Ipopt version: 3.12.7

The *Python/C++* version of the program used [*FEniCs*](https://fenicsproject.org/) for the generation of the FEM matrices and [*Ipopt*](https://projects.coin-or.org/Ipopt) for solving the optimization problem.

For using the program you first need to install *Ipopt* for the optimization. After the installation you need to build the *C++* code located in the `cpp/` subfolder. You may need to modify the provided `CMakeList.txt` file to match your configuration. In particular you need to set the path to your installation of *Ipopt*. The run:
```
 $ cd cpp/
 $ cmake .
 $ make
```
to build the program. This creates an executable called `heat` in the `cpp/` subfolder.

To run the the program use the file "main.py", i.e. type
```
 $ python3 main.py
```
In this script first the FEM matrices are generated. Then the *C++* executable you compiled previously is called repeatedly which handles the optimization of each open loop using *Ipopt*. The matrices are passed to the *C++* program by providing the path to where they are stored.
If you want to change the parameters (e.g. number of dofs) you just need to modify the python script.

For more information about the structure of the OCPs refer to the file `docu/docu.pdf`.
