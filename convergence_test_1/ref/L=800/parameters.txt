[param]
discretization_parameter = 300
alpha = 1
beta = 1
gamma = 1000
MPC_horizon = 800
eps = 0
y_ref = 0
u_ref = 0
u_upper = 0.25
u_lower = -0.25
y_upper = 0.15
y_lower = -0.15
w_upper = 1e+19
w_lower = -1e+19
convection = 1
open_values = 1
free_init_value = 1
function = 0.3 * sin(0.1 * i)
